from . import views
from django.urls import path

urlpatterns = [
    path("", views.home),
    path("kontakty", views.kontakty),
    path("neco", views.neco),
]
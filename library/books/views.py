from django.shortcuts import render

def home(request):
    return render(request, "books/home.html")
def kontakty(request):
    return render(request, "books/kontakty.html")
def neco(request):
    return render(request, "books/neco.html")
